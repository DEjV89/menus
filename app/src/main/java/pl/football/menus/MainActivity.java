package pl.football.menus;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by dkrezel on 2015-05-02.
 */
public class MainActivity extends ActionBarActivity {

	private TextView mTextView;
	private TextView mTextViewPopup;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_layout);

		// Przykład tworzenia menu kontekstowego
		mTextView = (TextView) findViewById(R.id.text_view_id);
		// zarejestrowanie menu kontekstowego dla widoku/widżetu mTextView
		registerForContextMenu(mTextView);

		// Przykład tworzenia menu popup
		mTextViewPopup = (TextView) findViewById(R.id.text_view_popup_id);
		mTextViewPopup.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				invokePopupMenu();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.option_menu_layout, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int mItemId = item.getItemId();
		if (mItemId == R.id.menu_item_3) {
			// Uruchom nową aktywność z Batch contextual actions
			Intent mIntent = new Intent(this, BatchMenuActivity.class);
			startActivity(mIntent);
		}
		return true;
	}

	@Override
	// Metoda uruchamiana w momencie tworzenia menu kontekstowego
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.context_menu_layout, menu);
	}

	@Override
	// Metoda uruchamiana przy wybraniu pozycji z menu kontekstowego
	public boolean onContextItemSelected(MenuItem item) {
		int mItemId = item.getItemId();
		switch (mItemId) {
			case R.id.context_menu_1 :
				Toast.makeText(this, "Using " + item.getTitle() + "..." , Toast.LENGTH_SHORT).show();
				break;
			case R.id.context_menu_2 :
				Toast.makeText(this, "Using " + item.getTitle() + "..." , Toast.LENGTH_SHORT).show();
				break;
			case R.id.context_menu_3 :
				Toast.makeText(this, "Using " + item.getTitle() + "..." , Toast.LENGTH_SHORT).show();
				break;
		}
		return true;
	}

	// Implementacja dla menu popup
	private void invokePopupMenu() {
		PopupMenu mPopupMenu = new PopupMenu(MainActivity.this, mTextViewPopup);
		mPopupMenu.getMenuInflater().inflate(R.menu.context_menu_layout, mPopupMenu.getMenu());

		mPopupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				Toast.makeText(MainActivity.this, "Using " + item.getTitle(), Toast.LENGTH_SHORT).show();
				return true;
			}
		});
		mPopupMenu.show();
	}
}
