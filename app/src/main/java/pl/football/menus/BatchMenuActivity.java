package pl.football.menus;

import android.app.ListActivity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dkrezel on 2015-05-02.
 */
public class BatchMenuActivity extends ListActivity {

	private ListView mListView;
	private List<String> mCityList = new ArrayList<>();
	private List<String> mPositionList = new ArrayList<>();
	private ListAdapter mListAdapter;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_view_layout);

		addCities();
		mListAdapter = new ListAdapter(this, mCityList);
		mListView = getListView();
		mListView.setAdapter(mListAdapter);

		addBatchContextualActions();
	}

	private void addBatchContextualActions() {
		mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
		mListView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

			@Override
			// Here you can do something when items are selected/de-selected,
			// such as update the title in the CAB
			public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
				String mItem = (String) mListView.getItemAtPosition(position);
				if (checked) {
					mPositionList.add(mItem);
				} else {
					mPositionList.remove(mItem);
				}
			}

			@Override
			// Inflate the menu for the CAB (Contextual Action Bar)
			public boolean onCreateActionMode(ActionMode mode, Menu menu) {
				MenuInflater mMenuInflater = mode.getMenuInflater();
				mMenuInflater.inflate(R.menu.context_menu_layout, menu);
				return true;
			}

			@Override
			// Here you can perform updates to the CAB due to
			// an invalidate() request
			public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
				return false;
			}

			@Override
			// Respond to clicks on the actions in the CAB
			public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
				switch (item.getItemId()) {
					case R.id.context_menu_1 :
						Toast.makeText(BatchMenuActivity.this, getString(R.string.not_implemented), Toast.LENGTH_SHORT).show();
						return true;
					case R.id.context_menu_2 :
						Toast.makeText(BatchMenuActivity.this, getString(R.string.not_implemented), Toast.LENGTH_SHORT).show();
						return true;
					case R.id.context_menu_3 :
						// Usuń zaznaczone pozycje
						deleteSelectedItems();
						mode.finish();
						return true;
				}
				return false;
			}

			@Override
			// Here you can make any necessary updates to the activity when
			// the CAB is removed. By default, selected items are deselected/unchecked.
			public void onDestroyActionMode(ActionMode mode) {

			}
		});
	}

	private void deleteSelectedItems() {
		for (String mItemName : mPositionList) {
			mListAdapter.remove(mItemName);
		}
		mListAdapter.notifyDataSetChanged();
		mPositionList.clear();
	}

	private void addCities() {
		mCityList.add("Wrocław");
		mCityList.add("Kraków");
		mCityList.add("Warszawa");
		mCityList.add("Poznań");
		mCityList.add("Gorzów Wielkopolski");
		mCityList.add("Częstochowa");
		mCityList.add("Łódź");
		mCityList.add("Lublin");
		mCityList.add("Gdańsk");
		mCityList.add("Szczecin");
		mCityList.add("Ełk");
		mCityList.add("Opole");

	}
}
