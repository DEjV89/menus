package pl.football.menus;

import android.app.ListActivity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dkrezel on 2015-05-02.
 */
public class ListAdapter extends ArrayAdapter<String> {

	private LayoutInflater mInflater;
	private TextView mCityName;
	private TextView mPopulation;
	private List<String> mElementList;

	public ListAdapter(Context mContext, List<String> mData) {
		super(mContext, R.layout.element_list_layout, mData);
		mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mElementList = new ArrayList<>(mData.size());
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		PositionViewHolder mPositionViewHolder;
		if (convertView == null) {

			convertView = mInflater.inflate(R.layout.element_list_layout, parent, false);
			mPositionViewHolder = new PositionViewHolder();

			mCityName = (TextView) convertView.findViewById(R.id.element_text1_id);
			mPopulation = (TextView) convertView.findViewById(R.id.element_text2_id);

			mPositionViewHolder.mCityName = this.mCityName;
			mPositionViewHolder.mPopulation = this.mPopulation;

			convertView.setTag(mPositionViewHolder);
		} else {
			mPositionViewHolder = (PositionViewHolder) convertView.getTag();
		}

		String mCityNameItem = getItem(position);

		mPositionViewHolder.mCityName.setText(mCityNameItem);

		return convertView;
	}

	private static class PositionViewHolder {
		public ImageView mImageView;
		public TextView mCityName;
		public TextView mPopulation;
	}
}
